// 1. Load data. 
use Equity
db.createCollection("CourseworkOne")
db.CourseworkOne.insert()

// 2. Count the total number of documents. 
db.CourseworkOne.find().count()

// 3. Find documents with symbol is MMM or ABT. 
db.CourseworkOne.find({$or:[{"Symbol":"MMM"},{"Symbol": "ABT"}]}).pretty()


// 4. Find all documents and sort them according to market price from low to high. 
db.CourseworkOne.find().pretty().sort({"MarketData.Price":1})

// 5. Aggregate all GICSSectors and calculate the average market price for each sector. 
db.CourseworkOne.aggregate([{$group:{_id:"$StaticData.GICSSector", priceAvg:{$avg:"$MarketData.Price"}}}])