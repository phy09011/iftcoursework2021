SELECT GICSSector, GICSIndustry, security, price_id, open, high, low, close, volume
FROM equity_prices, equity_static
WHERE equity_prices.symbol_id = equity_static.symbol and GICSSector='Financials';

SELECT security, GICSSector, GICSIndustry, cob_date, net_quantity, net_amount 
FROM portfolio_positions LEFT JOIN equity_static ON portfolio_positions.symbol=equity_static.symbol; 