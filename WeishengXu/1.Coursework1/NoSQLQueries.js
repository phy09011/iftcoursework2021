db.CourseworkOne.aggregate([{$match:{'MarketData.MarketCap':{'$ne':'NA'}}}, 
                            {$sort:{'MarketData.MarketCap':-1}},{$limit:10}])

db.CourseworkOne.aggregate([{$match:{}}, 
                            {$group:{_id:"$StaticData.GICSSector", 
                              average_beta:{$avg:"$MarketData.Beta"}}}, 
                            {$sort:{"average_beta":-1}}])
