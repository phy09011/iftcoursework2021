// 1. Distinct 
// 1.0 list all the industry types for those companies which belong to Information Technology sector
db.CourseworkOne.distinct("StaticData.GICSSubIndustry",{"StaticData.GICSSector":"Information Technology"})

// 2. AND
// 2.0 find which companies belong to Information Technology sector and in Technology Hardware, Storage & Peripherals industry
db.CourseworkOne.find({$and: [{"StaticData.GICSSector": {"$eq": "Information Technology"}},{"StaticData.GICSSubIndustry":{"$eq":"Technology Hardware, Storage & Peripherals"}}]}).pretty()

// 3. Find with conditions $eq, $ne, $gt, $lt, $gte, $lte
// 3.0 find all the stocks that is more volatile than the market, in other word, beta greater than 1. Theoretically, the beta of the market should be 1.
db.CourseworkOne.find({"MarketData.Beta":{"$gt": 1}})

// 3.1 find all those CounterCyclical stocks, in other words, beta smaller than zero. which should be rare but also valuable, as a way to help diversification in our portfoio and hedging the risk
db.CourseworkOne.find({"MarketData.Beta":{"$lt": 0}})

// 3.2 find all the stocks that is uncorrelated with the market, or has a beta value of zero. In fact, according to CAPM, zero beta means that we should not get any return exceed the market risk free rate
db.CourseworkOne.find({"MarketData.Beta":{"$eq": 0}})

// 4. Aggregate Method
// 4.0 For all companies in the financial sectors, aggregate them by subindustry and calculate each subindustry's average P/E ratio 
db.CourseworkOne.aggregate([
    {$match: {"StaticData.GICSSector": "Financials"} },
    {$group: {_id: "$StaticData.GICSSubIndustry", average: {$avg: "$FinancialRatios.PERatio"} } }])
