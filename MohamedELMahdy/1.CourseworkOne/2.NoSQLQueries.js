//Name: Mohamed ELMahdy
//Student_ID: 21161896
//Coursework: 1 - NoSQL queries

// Query1:

db.CourseworkOne.find(
{$and:[{'StaticData.GICSSector': "Financials"},{'MarketData.Price':{"$gte": 200}}]},
{'StaticData.Security': 1,'MarketData.Price': 1,'FinancialRatios.PayoutRatio': 1}).sort({'MarketData.Price': -1}).limit(5)

// Query2:


db.CourseworkOne.aggregate([
{$match:{'StaticData.GICSSector':'Health Care'}},
{$group:{_id:'$StaticData.GICSSubIndustry', Avg_MarketCap:{$avg:'$MarketData.MarketCap'}, Max_PayoutRatio:{$max:'$FinancialRatios.PayoutRatio'}}}
]).pretty()


