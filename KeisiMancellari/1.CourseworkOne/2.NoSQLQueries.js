//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
// UCL -- Big Data in Quantitative Finance
// Author  : Keisi Mancellari
// Topic   : Coursework 1 - NoSQL
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

// 1. Load Data
use Equity
db.createCollection("CourseworkOne")
db.CourseworkOne.insert()

// 2. General Information
db.CourseworkOne.find().limit(10).pretty()
db.CourseworkOne.find({}, {Symbol: 1, MarketData: 1}).pretty()
db.CourseworkOne.findOne({Symbol: "LOW"})
db.CourseworkOne.findOne({Symbol: "KSS"}, {MarketData: 1, FinancialRatios: 1})
db.CourseworkOne.distinct("StaticData.GICSSector")
db.CourseworkOne.find({"StaticData.GICSSector": "Materials"}).limit(5).pretty()

db.CourseworkOne.find().limit(10).sort({"MarketData.MarketCap":-1}).pretty()
db.CourseworkOne.find({"FinancialRatios.DividendYield": null}, {Symbol: 1})
db.CourseworkOne.find({"StaticData.GICSSector": {"$eq": "Energy"}}).pretty()
db.CourseworkOne.find({"StaticData.GICSSector": {"$eq": "Energy"}}, {Symbol: 1, MarketData: 1, FinancialRatios: 1}).pretty()
db.CourseworkOne.find({"StaticData.GICSSector": {"$ne": "Health Care"}}).pretty()
db.CourseworkOne.find({"StaticData.GICSSector": {"$ne": "Health Care"}}, {StaticData: 0}).pretty()
db.CourseworkOne.find({"MarketData.Beta": {"$lt": 1}}).pretty()
db.CourseworkOne.find({"FinancialRatios.PERatio": {"$gt": 30, "$lte": 40 }}).limit(10).pretty()
db.CourseworkOne.find({"FinancialRatios.PERatio": {"$gt": 30, "$lte": 40 }}).count()
db.CourseworkOne.find({"StaticData.GICSSubIndustry": {"$gt": "H"}})

db.CourseworkOne.find({"StaticData.GICSSector": {"$in": ["Health Care", "Industrials"]}})
db.CourseworkOne.find({"StaticData.GICSSector": {"$nin": ["Health Care", "Industrials"]}})

db.CourseworkOne.find({"FinancialRatios.PERatio": {"$gte": 35}, "StaticData.GICSSector": {"$ne": "Financials"}})
db.CourseworkOne.find({$or: [{"FinancialRatios.PERatio": {"$gte": 35}}, {"StaticData.GICSSector": {"$eq": "Industrials" }}]})

// 3. Manipulations

// Aggregate all Sectors and return the average Market Cap by Sector
db.CourseworkOne.aggregate([
	{$match: {} },
	{$group: {_id: "$StaticData.GICSSector", average: {$avg: "$MarketData.MarketCap"} } }])

// Aggregate all Sectors and return the average Beta only for the stocks that have a Market Cap larger than 100,000
db.CourseworkOne.aggregate([
	{$match: {"MarketData.MarketCap": {"$gte": 100000}}},
	{$group: {_id: "$StaticData.GICSSector", average: {$avg: "$MarketData.Beta"}}}])
	
// Aggregate all Subsectors within the Industrials Sector and return the PE Ratio average by SubSector  
db.CourseworkOne.aggregate([
	{$match: {"StaticData.GICSSector": "Industrials"}},
	{$group: {_id: "$StaticData.GICSSubIndustry", total: {$avg: "$FinancialRatios.PERatio"}}}])

// Aggregate all Sectors within the Health Care Sector and return the Market Cap sum for the Sector
db.CourseworkOne.aggregate([
	{$match: {"StaticData.GICSSector": "Health Care"} },
	{$group: {_id: "$StaticData.GICSSector", total: {$sum: "$MarketData.MarketCap"}}}])
