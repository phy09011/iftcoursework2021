#Omit openning database part
#First query: All information technology company, whose equity price close > open at 20-Jul-2021, and order by increasement.
#The following columns are: security, open, close, close-open
sqlite> select equity_static.security,equity_prices.open,equity_prices.close, equity_prices.close - equity_prices.open from equity_prices join equity_static on equity_prices.symbol_id = equity_static.symbol where equity_prices.close - equity_prices.open > 0 and equity_prices.cob_date == '20-Jul-2021' and equity_static.GICSSector == 'Information Technology' order by equity_prices.close - equity_prices.open desc;
Intuit Inc.|500.06|508.56|8.5
Autodesk Inc.|287.9|296.12|8.22000000000003
KLA-Tencor Corp.|295.3|302.77|7.46999999999997
Mastercard Inc.|367|374.39|7.38999999999999
ANSYS|345|350.87|5.87
IPG Photonics Corp.|203.01|208.14|5.13
Microchip Technology|132.34|137.03|4.69
Fortinet|259.49|263.84|4.34999999999997
FleetCor Technologies Inc|246.63|250.59|3.96000000000001
Alliance Data Systems|94.16|97.55|3.39
Adobe Systems Inc|605.39|608.72|3.33000000000004
F5 Networks|183.56|186.68|3.12
TE Connectivity Ltd.|132.53|135.52|2.99000000000001
Applied Materials Inc.|129.1954|131.97|2.77459999999999
Apple Inc.|143.46|146.15|2.69
Synopsys Inc.|275.25|277.89|2.63999999999999
Qorvo|187.07|189.7|2.63
PayPal|295.515|298.07|2.55500000000001
Jack Henry & Associates Inc|171.1|173.63|2.53
Global Payments Inc.|186.62|188.75|2.13
Accenture plc|310|312.06|2.06
Visa Inc.|240.64|242.7|2.06
Fiserv Inc|106.87|108.84|1.97
Fidelity National Information Services|142.98|144.83|1.85000000000002
Western Digital|62.8|64.64|1.84
Cadence Design Systems|137.97|139.79|1.81999999999999
Broadridge Financial Solutions|167.36|169.15|1.78999999999999
Skyworks Solutions|187.65|189.42|1.76999999999998
Analog Devices, Inc.|159.34|160.91|1.56999999999999
Oracle Corp.|87.1|88.66|1.56
Maxim Integrated Products Inc|96.04|97.52|1.47999999999999
Xilinx|129.66|131.1|1.44
Salesforce.com|238.7|240.11|1.41000000000003
Microsoft Corp.|278.03|279.32|1.29000000000002
Seagate Technology|84.25|85.49|1.23999999999999
Keysight Technologies|155.6|156.59|0.990000000000009
Cognizant Technology Solutions|66.68|67.61|0.929999999999993
Gartner Inc|254.77|255.68|0.909999999999997
DXC Technology|37.73|38.63|0.900000000000006
Amphenol Corp|67.79|68.61|0.819999999999993
Motorola Solutions Inc.|217.82|218.58|0.760000000000019
Verisign Inc.|228.69|229.43|0.740000000000009
Micron Technology|74.66|75.39|0.730000000000004
Texas Instruments|187.0563|187.77|0.713700000000017
Broadcom|467.89|468.59|0.699999999999989
Paychex Inc.|109.9|110.46|0.559999999999988
Corning Inc.|39.57|40.11|0.539999999999999
NetApp|77.71|78.24|0.530000000000001
QUALCOMM Inc.|139.0201|139.55|0.529899999999998
HP Inc.|27.74|28.21|0.470000000000002
Automatic Data Processing|202.78|203.14|0.359999999999985
Juniper Networks|26.78|27.12|0.34
Intel Corp.|54.915|55.24|0.325000000000003
Citrix Systems|114.41|114.72|0.310000000000002
Cisco Systems|53.28|53.58|0.299999999999997
Hewlett Packard Enterprise|13.69|13.97|0.280000000000001
Western Union Co|22.82|23|0.18
Advanced Micro Devices Inc|87.09|87.11|0.019999999999996

#Second query: the traderman who is funded by US S&P500 Low Volatility, currency is USD and limit_type is short.
#The columns are: trader_name, limit_id, limit_category, limit_amount, limit_start, limit_end.
sqlite> select trader_static.trader_name, trader_limits.limit_id, trader_limits.limit_category, trader_limits.limit_amount, trader_limits.limit_start, trader_limits.limit_end from trader_limits join trader_static on trader_static.trader_id=trader_limits.trader_id where trader_static.fund_name == 'US S&P500 Low Volatility' and trader_limits.currency == 'USD' and trader_limits.limit_type == 'short';
Matt Red|MRH5231shortLIVE|consideration|5000000|16-Apr-2020|
Matt Red|MRH5231short18Apr2020|consideration|100000000|15-Dec-2014|18-Apr-2020
