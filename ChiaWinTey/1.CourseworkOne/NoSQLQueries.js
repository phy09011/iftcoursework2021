//Query 1 - As the portfolio holder invested in the Information Technology, therefore have a look on other company's performance within this sector.

db.CourseworkOne.find({"StaticData.GICSSector":{"$eq":"Information Technology"}})

//Query 2 - Find out the performance of the company which are invested by the portfolio holder.

db.CourseworkOne.find({$or:[{Symbol:{"$eq":"ORCL"}},{Symbol:{"$eq":"ACN"}},{Symbol:{"$eq":"AMAT"}},{Symbol:{"$eq":"AVGO"}},
{Symbol:{"$eq":"AAPL"}},{Symbol:{"$eq":"IBM"}},{Symbol:{"$eq":"NVDA"}}]})

//Query 3 - Find the average PE ratio of the whole industry to compare the performance of the company.

db.CourseworkOne.aggregate([ {$match: {"StaticData.GICSSector": "Information Technology"} }, {$group: {_id: "$StaticData.GICSSector", AveragePERatio: {$avg: "$FinancialRatios.PERatio"} } }])